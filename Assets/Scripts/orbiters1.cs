﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class orbiters1 : MonoBehaviour
{
    public int orbitersCount = 5000;
    public int maxStartRadius = 200;
    public int gravityMultiplyer = 100;
    public int maxStartTilt = 15;
    static public GameObject[] spheres;
    public Material[] mats;
    public Material trail;
    public const float G = 6.67422f;

    private void Awake()
    {
        spheres = new GameObject[orbitersCount];
    }
    // Start is called before the first frame update
    void Start()
    {
        var rb = this.GetComponent<Rigidbody>();
        rb.mass = (float)(Math.Pow(this.transform.localScale.x, 3) * Math.PI * (4 / 3) * 1.408f);
        spheres = createObjects(orbitersCount, maxStartRadius, maxStartTilt);
    }

    private GameObject[] createObjects(int sphereCount, int maxRadius, int maxTilt)
    {
        var sunrad = this.transform.localScale.x;
        var sphs = new GameObject[sphereCount];
        var sphereToCopy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Rigidbody rb = sphereToCopy.AddComponent<Rigidbody>();
        rb.useGravity = false;
        var ball = sphereToCopy.GetComponent<SphereCollider>();
        ball.enabled = false;

        for (int i = 0; i < sphereCount; i++)
        {
            var sp = GameObject.Instantiate(sphereToCopy);
            sp.transform.position = this.transform.position +
            new Vector3(Random.Range(-maxRadius, maxRadius),
                        Random.Range(-maxTilt, maxTilt),
                        Random.Range(-maxRadius, maxRadius));
            sp.transform.localScale *= Random.Range((sunrad / 1000), (sunrad / 10));
            sp.GetComponent<Renderer>().material = mats[Random.Range(0, mats.Length)];
            TrailRenderer tr = sp.AddComponent<TrailRenderer>();
            tr.time = 0.7f;
            tr.startWidth = 0.4f;
            tr.endWidth = 0;
            tr.material = trail;
            tr.startColor = new Color(1, 1, 0, 0.1f);
            tr.endColor = new Color(0, 0, 0, 0);
            sp.GetComponent<Rigidbody>().mass = calculateMass(sp, sunrad);
            sphs[i] = sp;
        }
        GameObject.Destroy(sphereToCopy);

        return sphs;
    }

    private float calculateMass(GameObject sp, float sunrad)
    {
        float rocky = 1.225f;
        float gassy_giant = 3.2955f;
        float mass;

        if ( sp.transform.localScale.x >= (sunrad / 400))
        {
            mass = (float)(Math.Pow(sp.transform.localScale.x, 3) * Math.PI * (4 / 3) * gassy_giant);
        }
        else if (sp.transform.localScale.x <= (sunrad / 500))
        {
            mass = (float)(Math.Pow(sp.transform.localScale.x, 3) * Math.PI * (4 / 3) * rocky);
        }
        else
        {
            if(Random.Range(0, 1) >= 0.5)
            {
                mass = (float)(Math.Pow(sp.transform.localScale.x, 3) * Math.PI * (4 / 3) * gassy_giant);
            }
            else
            {
                mass = (float)(Math.Pow(sp.transform.localScale.x, 3) * Math.PI * (4 / 3) * rocky);
            }
        }

        return mass;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject s in spheres)
        {
            s.transform.GetComponent<Rigidbody>().AddRelativeForce(s.transform.forward, ForceMode.Acceleration);
            s.transform.Rotate(new Vector3(0f, 0.1f, 0f));
            Attract(s);
            Vector3 gravVector = createGravity(this, s);
            s.transform.GetComponent<Rigidbody>().AddForce(gravVector);
        }
        this.transform.Rotate(new Vector3(0f, 15f, 0f));
    }

    void Attract(GameObject obj)
    {
        foreach (GameObject s in spheres)
        {
            if (obj == s)
            {
                return;
            }
            else
            {
                Vector3 gravVector2 = createGravity(obj, s);
                s.transform.GetComponent<Rigidbody>().AddRelativeForce(gravVector2);
            }
        }
    }

    private Vector3 createGravity(GameObject obj, GameObject s)
    {
        var m1 = obj.GetComponent<Rigidbody>();
        var m2 = s.GetComponent<Rigidbody>();

        Vector3 diff = obj.transform.position - s.transform.position;
        float dist = diff.magnitude;
        if (dist == 0)
        {
            dist = 1f;
        }
        Vector3 gravityDirec = diff.normalized;
        float gravity = (G * this.gravityMultiplyer) * m1.mass * m2.mass / (dist * dist);

        return (gravityDirec * gravity);
    }

    Vector3 createGravity(orbiters1 a, GameObject s)
    {

        var m1 = s.GetComponent<Rigidbody>();
        var m2 = a.GetComponent<Rigidbody>();

        Vector3 diff = a.transform.position - s.transform.position;
        float dist = diff.magnitude;
        Vector3 gravityDirec = diff.normalized;
        float gravity = (G * this.gravityMultiplyer ) * m1.mass * m2.mass / (dist * dist);

        return (gravityDirec * gravity);
    }
}
