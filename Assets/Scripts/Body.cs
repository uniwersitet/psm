﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Body : MonoBehaviour
{
    public int sphereCount = 2;
    public int maxRadius = 200;
    static public GameObject[] spheres;
    public Material[] mats;
    public Material trail;
    public const float G = 6.67422f;

    private void Awake()
    {
        spheres = new GameObject[sphereCount];
    }
    // Start is called before the first frame update
    void Start()
    {
        spheres = createObjects(this.sphereCount, this.maxRadius);
        var rb = this.GetComponent<Rigidbody>();
        rb.mass = (float)(Math.Pow(this.transform.localScale.x, 3) * Math.PI * (4 / 3));
        this.transform.position = this.transform.position +
             new Vector3(Random.Range(-this.maxRadius, this.maxRadius),
                         Random.Range(-15, 15),
                         Random.Range(-this.maxRadius, this.maxRadius));
    }

    private GameObject[] createObjects(int sphereCount, int maxRadius)
    {
        var sphs = new GameObject[sphereCount];
        var sphereToCopy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Rigidbody rb = sphereToCopy.AddComponent<Rigidbody>();
        rb.useGravity = false;
        var ball = sphereToCopy.GetComponent<SphereCollider>();
        ball.enabled = false;

        for (int i = 0; i < sphereCount; i++)
        {
            var sp = GameObject.Instantiate(sphereToCopy);
            sp.transform.position = this.transform.position +
            new Vector3(Random.Range(-maxRadius, maxRadius),
                        Random.Range(-1, 1),
                        Random.Range(-maxRadius, maxRadius));
            sp.transform.localScale *= Random.Range(this.transform.localScale.x / 2, this.transform.localScale.x * 2);
            sp.GetComponent<Renderer>().material = mats[Random.Range(0, mats.Length)];
            TrailRenderer tr = sp.AddComponent<TrailRenderer>();
            tr.time = 0.1f;
            tr.startWidth = 0.1f;
            tr.endWidth = 0;
            tr.material = trail;
            tr.startColor = new Color(1, 1, 0, 0.1f);
            tr.endColor = new Color(0, 0, 0, 0);
            sp.GetComponent<Rigidbody>().mass = (float)(Math.Pow(sp.transform.localScale.x, 3) * Math.PI * (4/3));
            sphs[i] = sp;
        }
        GameObject.Destroy(sphereToCopy);

        return sphs;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject s in spheres)
        {
            var m1 = s.GetComponent<Rigidbody>();
            var m2 = this.GetComponent<Rigidbody>();

            Vector3 diff = this.transform.position - s.transform.position;
            float dist = diff.magnitude;
            Vector3 gravityDirec = diff.normalized;
            float gravity = G * m1.mass * m2.mass / (dist * dist);
            Vector3 gravVector = (gravityDirec * gravity);

            Vector3 torque = new Vector3(0f, 100f, 0f);

            s.transform.GetComponent<Rigidbody>().AddForce(s.transform.forward, ForceMode.Acceleration);
            s.transform.Rotate(torque);
            Attract(s);
            s.transform.GetComponent<Rigidbody>().AddForce(gravVector);
        }

        Attract(this);
    }

    void Attract(GameObject obj)
    {
        foreach (GameObject s in spheres)
        {
            if (obj == s)
            {
                return;
            }
            else
            {
                var m1 = s.GetComponent<Rigidbody>();
                var m2 = obj.GetComponent<Rigidbody>();

                Vector3 diff = obj.transform.position - s.transform.position;
                float dist = diff.magnitude;
                if (dist == 0)
                {
                    dist = 1f;
                }
                Vector3 gravityDirec = diff.normalized;
                float gravity = G * m1.mass * m2.mass / (dist * dist);
                Vector3 gravVector2 = (gravityDirec * gravity);

                s.transform.GetComponent<Rigidbody>().AddForce(gravVector2);
            }
        }
    }

    void Attract(Body orb)
    {
        foreach (GameObject s in spheres)
        {
            var m1 = orb.GetComponent<Rigidbody>();
            var m2 = s.GetComponent<Rigidbody>();

            Vector3 diff = s.transform.position - orb.transform.position;
            float dist = diff.magnitude;
            Vector3 gravityDirec = diff.normalized;
            float gravity = G * m1.mass * m2.mass / (dist * dist);
            Vector3 torque = new Vector3(0f, 100f, 0f);
            Vector3 gravVector = (gravityDirec * gravity);

            orb.transform.Rotate(torque);
            orb.transform.GetComponent<Rigidbody>().AddForce(gravVector);
            s.transform.GetComponent<Rigidbody>().AddForce(s.transform.forward, ForceMode.Acceleration);
        }
    }
}

